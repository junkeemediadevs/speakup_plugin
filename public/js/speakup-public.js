(function( $ ) {
	'use strict';

	$( window ).load(function() {

		$("#modal__speakup--form").submit(function( event ) {

			event.preventDefault();

			var $form = $(this); 

			var user_name = $( "input[name=input-name]" ).val();
			var user_contact = $( "input[name=input-contact]" ).val();

			var article_url = $( "input[name=input-article-url]" ).val();
			var article_id = $( "input[name=input-article-id]" ).val();
			var article_title = $( "input[name=input-article-name]" ).val();
			var article_author = $( "input[name=input-article-author]" ).val();

			var user_reason = $( "textarea#speakup-user-reason" ).val();
			var user_contribution = $( "textarea#speakup-user-contribution" ).val();

			var text = '';

			text += 'Author: ' + article_author + '\n';
			text += 'Article Title: ' + article_title + '\n';
			text += 'Article URL: ' + article_url + '\n';
			text += 'Article ID: ' + article_id + '\n' + '\n';
			text += 'User Name: ' + user_name + '\n';
			text += 'User Contact: ' + user_contact + '\n' + '\n';
			text += 'Reason for contact: ' + '\n' + user_reason + '\n' + '\n';
			text += 'Contribution details: ' + '\n' + user_contribution + '\n';

			$.ajax({
			    data: 'payload=' + JSON.stringify({
			        "text": text
			    }),
			    dataType: 'json',
			    processData: false,
			    type: 'POST',
			    url: $form.attr('action')
			})

			$(".modal__speakup--thanks").fadeIn();
	        $(".modal__speakup--form").hide();

	    });

	    $(".cta__speakup--another").click(function(){
	        $(".modal__speakup--input, .modal__speakup--checkbox, .modal__speakup--textarea").val('');
	        $(".modal__speakup--thanks").hide();
	        $(".modal__speakup--form").fadeIn();
	    });
	});

	
	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );
