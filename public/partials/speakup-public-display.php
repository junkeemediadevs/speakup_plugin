<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Speakup
 * @subpackage Speakup/public/partials
 */
?>

<?php

$webhook = get_option('slack_webhook');

global $post;

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<a href="#modal__speakup" class="cta cta__speakup" title="Provide feedback and contribute to our stories" rel="modal:open">speak up - contribute to this article</a>

<!-- Speakup Modal -->
<div id="modal__speakup" class="modal__speakup">

    <form class="modal__speakup--form" action="<?php echo $webhook; ?>" method="post" id="modal__speakup--form">
        <div class="modal__speakup--title">Add your voice</div>
        <p>We’ve made it even easier for you provide feedback and contribute to our stories. Is there something you want to add? Maybe a link, video or tweet we missed? Just fill out the form below.</p>
        <p>Even if you just want to tell us we got it wrong, we’re after your contributions. If they add to the story we’ll use them to liven up the article.</p>

        <span class="col">

        	<input type="hidden" name="input-article-url" id="input-article-url" value="<?php the_permalink(); ?> ">
           	<input type="hidden" name="input-article-id" id="input-article-id" value="<?php echo $post->ID; ?>">
           	<input type="hidden" name="input-article-name" id="input-article-name" value="<?php the_title(); ?>">
           	<input type="hidden" name="input-article-author" id="input-article-author" value="<?php echo get_the_author_meta('display_name', $post->post_author); ?>">

            <input class="modal__speakup--input" type="text" name="input-name" id="input-name" placeholder="Enter your full name">
            <input class="modal__speakup--input" type="text" name="input-contact" id="input-contact" placeholder="Contact preference (twitter/email/mobile)">
            <span class="checkbox"><input class="modal__speakup--checkbox" type="checkbox" name="checkbox-notifyme"> Notify me when the change is made</span>
            <span class="checkbox"><input class="modal__speakup--checkbox" type="checkbox" name="checkbox-attributeme"> Add my name to the change</span>
            <textarea class="modal__speakup--textarea" id="speakup-user-reason" name="textarea-reason" placeholder="What is the reason for your contribution"></textarea>
        </span>
        <span class="col">
            <textarea class="modal__speakup--textarea textarea-contribution" id="speakup-user-contribution" name="textarea-contribution" placeholder="Add your contribution"></textarea>
            <input class="modal__speakup--submit" type="submit" value="Submit">
        </span>
    </form>

    <span class="modal__speakup--thanks">
        <div class="modal__speakup--title">Thanks for Speaking Up!</div>
        <p>Your contribution has been sent to our editorial team.</p>
        <a class="cta__speakup--another" href="#">Got something else to contribute?</a>
    </span>

</div>