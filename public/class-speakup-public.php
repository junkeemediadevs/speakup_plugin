<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Speakup
 * @subpackage Speakup/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Speakup
 * @subpackage Speakup/public
 * @author     Your Name <email@example.com>
 */
class Speakup_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $speakup    The ID of this plugin.
	 */
	private $speakup;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $speakup       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $speakup, $version ) {

		$this->speakup = $speakup;
		$this->version = $version;

		add_filter('the_content', array(&$this, 'speakupForm'));
		
	}


	function speakupForm($content) {

		if(is_single()) {

			ob_start();
			include plugin_dir_path( __FILE__ ) . 'partials/speakup-public-display.php';
			$speakupFrom = ob_get_clean();

			$content = $content . $speakupFrom;

		}

		return $content;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Speakup_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Speakup_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( 'speakup-fonts', 'https://fonts.googleapis.com/css?family=Roboto|Oswald' );
		
		wp_enqueue_style( 'speakup-css', plugin_dir_url( __FILE__ ) . 'css/speakup-public.css' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Speakup_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Speakup_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js');
		wp_enqueue_script( 'jquery-modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.5.8/jquery.modal.min.js');

		wp_enqueue_script( 'speakup-js', plugin_dir_url( __FILE__ ) . 'js/speakup-public.js' );

	}

}
