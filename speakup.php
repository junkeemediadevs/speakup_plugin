<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Speakup
 *
 * @wordpress-plugin
 * Plugin Name:       Speakup
 * Plugin URI:        http://example.com/speakup-uri/
 * Description:       Give user a voice! Easy to use user contribution system
 * Version:           1.0.0
 * Author:            Junkee Media
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       speakup
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-speakup-activator.php
 */
function activate_speakup() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-speakup-activator.php';
	Speakup_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-speakup-deactivator.php
 */
function deactivate_speakup() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-speakup-deactivator.php';
	Speakup_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_speakup' );
register_deactivation_hook( __FILE__, 'deactivate_speakup' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-speakup.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_speakup() {

	$plugin = new Speakup();
	$plugin->run();

}
run_speakup();
