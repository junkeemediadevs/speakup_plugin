# SPEAKUP #

**Type:** Wordpress Plugin

**Requirements:** Wordpress, Slack Login and a Slack Web Hook

**Version:** 1.0

## The Problem ##

In today’s digital news environment journalists are regularly soliciting reader feedback and contributions.

But those contributions are made across a range of platforms including email, Facebook, Twitter and on-site comments. Some of those tools are inaccessible to many readers and most of them require users to go off-platform. 

It’s not easy for editors and journalists to collate feedback, sift through what’s valuable and either add it to their story, or use it for a follow-up.

It’s also difficult for audiences themselves to know where to make contributions, or how those contributions will be used.

## The Solution ##

SpeakUp is a WordPress plugin that gives audiences an easy way to contribute to news stories once they’ve gone live, and delivers those contributions in a format that is easy for editors and journalists to evaluate. 

SpeakUp is a call to action button that lives on news stories. It allows users to provide feedback and contributions to certain sections of a news story or the entire article. 

Through an intuitive and engaging UI readers can submit text, video, image or social media contributions. Readers will also have the option of providing their email and Twitter handle, so they can be contacted or notified if their feedback has been incorporated into a story.

Editors and journalists will receive the contributions in a dedicated Slack channel. They will immediately see the relevant story title, URL, section of the story and the contribution.

Editors can then choose to integrate the contributions directly into the story, for example by adding additional information, videos, image and Tweets. Those additional pieces of information and content will be stylised in a different format to indicate they are reader contributions.

By seeing how reader contributions can add to and change a story, other readers will be encouraged to make their own contributions.

## How SpeakUp Can Be Used ##

Stories can be updated to include reader contributions made through SpeakUp and journalists can choose use contributions as the basis for follow-up stories. 

In some instances journalists will write articles for the explicit purpose of seeking contributions eg. Breaking news stories often solicit contributions, stories that seek first hand experiences.

SpeakUp can be used in those instances to allow readers to easily make a contribution to a story and allow journalists to assess them more easily in one simple platform.

## Installation ##

1. Activate Plugin
2. Setup Slack Webhook