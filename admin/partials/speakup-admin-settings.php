<?php if ($attr['update_message']): ?>
    <div class="updated"><p><strong><?php echo $attr['update_message']; ?></strong><p></div>
<?php endif; ?>

<?php if ($attr['error_message']): ?>
    <div class="error"><p><strong><?php echo $attr['error_message']; ?></strong><p></div>
<?php endif; ?>

<div id="speakup-admin" class="wrap">

    <h2>SpeakUp Settings</h2>

    <form name="speakup-settings" method="post" action="<?php echo $attr['action']; ?>">

        <p>
            <span class="label">Slack Webhook:</span><br />
            <input type="text" name="slack_webhook" value="<?php echo $attr['slack_webhook']; ?>" size="100" />
        </p>

        <hr />

        <p class="submit">
            <input type="submit" name="Submit" class="button-primary" value="Save Changes" />
        </p>

    </form>

</div>
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>