<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Speakup
 * @subpackage Speakup/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Speakup
 * @subpackage Speakup/admin
 * @author     Your Name <email@example.com>
 */
class Speakup_Admin {

    const SLACK_WEBHOOK = 'slack_webhook';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $speakup    The ID of this plugin.
	 */
	private $speakup;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $webhook;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $speakup       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $speakup, $version ) {

		$this->speakup = $speakup;
		$this->version = $version;


	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Speakup_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Speakup_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->speakup, plugin_dir_url( __FILE__ ) . 'css/speakup-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Speakup_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Speakup_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->speakup, plugin_dir_url( __FILE__ ) . 'js/speakup-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
     * Register the menu for the admin area.
     *
     * @since    1.0.0
     */
    public function admin_menu_page() {
        add_menu_page(
        	'Speakup', 
        	'Speakup', 
        	'manage_options', 
        	'speakup-settings', 
        	array($this, 'admin_settings_page'),
        	'dashicons-megaphone'
        );
    }

    /**
     * Settings
     *
     * @since    1.0.0
     */
    public function admin_settings_page() {

        $formAction = add_query_arg(array(
            'page' => 'speakup-settings',
        ), admin_url('admin.php'));

        $attr = $this->init_settings();

        $attr['action'] = $formAction;
       
        // See if the user has posted us some information
        // If they did, this hidden field will be set to 'Y'
        if (isset($_POST['Submit']) && $_POST['Submit'] == 'Save Changes' ) {
            $updates = $this->update_settings();
            if($updates) {
                $attr['update_message'] = "Settings successfully saved";
            } else {
                $attr['error_message'] = "Something went wrong with updates.";
            }
        }

        $attr = $this->get_settings($attr);
        require_once plugin_dir_path( __FILE__ ) . 'partials/speakup-admin-settings.php';
    }

    public function admin_test_page() {

    	$response = $this->slack_test();
    	require_once plugin_dir_path( __FILE__ ) . 'partials/speakup-admin-test.php';
    }

    private function update_settings() {
        // Read their posted value
        $slack_webhook = $_POST['slack_webhook'];
        
        // Save settings
        update_option( self::SLACK_WEBHOOK, $slack_webhook);

        return true;
    }

    private function get_settings($attr) {
        $attr['slack_webhook'] = get_option(self::SLACK_WEBHOOK);
        return $attr;
    }

    private function init_settings() {
        $attr = array(
            'update_message' => null,
            'error_message' => null,
            'action' => null,
            'slack_webhook' => null
        );
        return $attr;
    }

    /** SLACK **/

    private function slack_test()
    {

    	$data = array('text' => 'hello world');
    	$json =  json_encode($data);
    	
    	$response = $this->slack_push($json);
    	return $response;
    }

    private function slack_push($json)
    {
    	$webhook = get_option(self::SLACK_WEBHOOK);

        $headers= array('Accept: application/json','Content-Type: application/json');
        $ch = curl_init($webhook);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$json);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
     
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }



}
